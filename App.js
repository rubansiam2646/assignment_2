import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ActivityIndicator,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useState} from 'react';
import {useEffect} from 'react';

const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getMovies = async () => {
    try {
      const response = await fetch(
        'https://tinyfac.es/api/data?limit=50&quality=0',
      );
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getMovies();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Icon
          name="chevron-left"
          size={20}
          color="#ff6876"
          style={{marginLeft: 10, marginTop: 5}}
        />
        <Text style={styles.font1}>Back</Text>
        <Text style={styles.font2}>Super Like me</Text>
        <Text style={styles.font3}>SELECT</Text>
      </View>

      <View style={styles.cont}>
        <TextInput style={styles.input1} placeholder="search" />
        <Icon
          name="search"
          size={20}
          color="#eff0f4"
          style={{marginLeft: 10, position: 'absolute', top: 20, left: '32%'}}
        />
      </View>
      <View style={styles.cont1}>
        <Text style={styles.font4}>All matches</Text>
        <View style={styles.icons1}>
          <Text style={styles.hh}>1h</Text>
          <Icon name="list" size={20} color="#364451" style={{}} />
          <Icon
            name="th-large"
            size={20}
            color="#364451"
            style={{marginLeft: 10}}
          />
        </View>
      </View>

      <View style={{marginTop: 20, backgroundColor: '#fffff'}}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            keyExtractor={({id}, index) => id}
            renderItem={({item}) => (
              <View style={styles.fcont}>
                <Image
                  source={{uri: item.url}}
                  style={{
                    width: 50,
                    height: 50,
                    borderWidth: 4,
                    borderRadius: 30,
                    borderColor: '#fff',

                    margin: 8,
                    marginLeft: 20,
                  }}></Image>
                <View style={styles.flat_t}>
                  <Text style={styles.flat}>{item.first_name}</Text>
                  <Text style={styles.flat1}>{item.gender}</Text>
                  <View>
                    <Icon
                      name="star"
                      size={20}
                      color="#364451"
                      style={{marginLeft: 200, marginTop: -30}}
                    />
                  </View>
                </View>
              </View>
            )}
          />
        )}
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    flexDirection: 'row',
    marginTop: 10,
  },
  font1: {
    color: '#ff6876',
    fontSize: 20,
    marginLeft: 10,
  },
  font2: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 30,
  },
  font3: {
    color: '#ff6876',
    fontSize: 20,
    marginLeft: 'auto',
    marginRight: 10,
  },
  cont: {
    backgroundColor: '#eff0f4',
    marginTop: 20,
  },
  input1: {
    borderStyle: 'solid',
    borderColor: 'white',
    backgroundColor: 'white',
    height: 40,
    width: '85%',
    margin: 12,
    borderWidth: 1,
    padding: 10,
    textAlign: 'center',
    fontSize: 20,
    marginLeft: 25,
  },
  cont1: {
    flexDirection: 'row',
    marginTop: 20,
    backgroundColor: '#f7f8fb',
  },
  font4: {
    color: 'black',
    marginLeft: 10,
  },
  icons1: {
    marginLeft: 'auto',
    flexDirection: 'row',
    marginRight: 30,
  },
  hh: {
    color: '#364451',
    marginRight: 10,
  },
  flat: {
    fontSize: 20,
    color: 'black',
    marginLeft: 50,
  },
  flat1: {
    fontSize: 15,
    marginLeft: 50,
  },
  fcont: {
    flexDirection: 'row',
    backgroundColor: 'red ',

    borderWidth: 1,
    borderColor: '#ffffff',
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 20,
    height: 70,
    width: '80%',
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
  },
  flat_t: {
    backgroundColor: '#ffffff',
    width: '100%',
  },
});
